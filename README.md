# CS402-Systems Programming Lab1-C Programming Basics

## 1.Goal

Describe the structure of the code and how it worksn

## 2.C version

Currently, code should target C11, i.e.

## 3.Files

//C file

CS401_SP_lab1.c	

//Head file

CS401_SP_lab1.h	

## 4.Head file

### 4.1.Manifest Constant

//The numbers of the employees.

#define SIZE 1024  

//assume that no first nor last name is longer than 64 characters.

#define MAXNAME 64

//Print the length of *.

#define WIDTH 40

//Print the length of -.

#define WIDTH2 43

### 4.2.Define a struct

struct person
{
    int id;
    char name[MAXNAME];
    char lname[MAXNAME];
    int salary;
};

### 4.3.Define function

// compare function

int Com (const void *a, const void *b)

{

        struct person *c = (struct person *) a;
        struct person *d = (struct person *) b;
        return (*c).id - (*d).id;    
}

// A function that prints a certain number of *

void starbar(void)

{

        int count;
        for (count = 1; count <= WIDTH; count++)
        putchar('*');
        putchar('\n');
}

// Functions to read values

int read_int(int *address)

{

        if(scanf("%d",address) == 0)
            return -1;
        return 0;
}

// Functions of binary search

int binary_search(const int arr[],int l,int r,int x)

{

        if(r>=l)
        {
                int mid = l + (r - l) / 2;
            /* if the element is present at the middle
               itself*/ 
            if(arr[mid] == x)
                return mid;
            /* if element is smaller than mid ,then
               it can only be present in left subarray*/
            if(arr[mid]>x)
                return binary_search(arr, l, mid - 1, x);
            /* else the element can only be present
               in right subarray*/
            return binary_search(arr, mid + 1, r, x);
        }
        /* we reach here when element is not
           present in array*/
        return -1;
}

// Function of lastname.when find the goal return key.

int search_lastname(struct person employee[SIZE],int n,char surname[])

{

        for (int i = 0; i < n;i++)
        {
            if(strcmp(employee[i].lname,surname) == 0)
            {
                return i;
            }
        }
        return -1;
}

// Functions of print data by key.

void print_by_key(struct person employee[SIZE],int key)

{

        //Given structure and id,this function prints the employee record if it exists
        starbar2();
        printf("Name                       SALARY    ID     \n");
        printf("%-10s %-10s %10d %10d\n", employee[key].name, employee[key].lname, employee[key].salary, employee[key].id);
        starbar2();
}

## 5.C file

### 5.1.Include head file

#include <stdlib.h>

#include <stdio.h>

#include <string.h>

#include "readfile.h"

### 5.2.Main

// read data form txt

if(open_file(fname) == -1)

{

        printf("error reading file.\n");
        return -1;
}

        FILE *fp = fopen(fname, "r");
        while(!feof(fp)
{

        fscanf(fp, "%d %s %s %d\n", &employee[n].id, &employee[n].name, &employee[n].lname, &employee[n].salary);
        array[n] = employee[n].id;
        n++;
}

close_file(fp);

// sort the employees

qsort(employee, 14, sizeof(struct person), Com);

// create menu

while(1){...}

## 6.Execute

Your program will take one command line argument, which is the name of the input file containing employee information. 
For example, telling your program to load the employee data stored in the file "small.txt" would look like:

./CS402_SP_Lab1 input.txt

The menu will print on the terminal,look like:

Employee DB Menu:
****************************************

(1) Print the Database

(2) Lookup by ID

(3) Lookup by Last Name

(4) Add an Employee

(5) Quit

****************************************

Enter your option:

****************************************

// input 1

It will show the employees list.

// input 2

It will search for employees by ID,and print the employee an the terminal

// input 3

It will search for employees by lastname,and print the employee an the terminal

// input 4

Add new employee information at the end of the database.

// input 5

quit.