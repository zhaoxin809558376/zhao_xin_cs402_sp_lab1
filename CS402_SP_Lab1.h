#define SIZE 1024
#define MAXNAME 64
#define WIDTH 40
#define WIDTH2 43

/*define a person*/
struct person
{
    int id;
    char name[MAXNAME];
    char last_name[MAXNAME];
    int salary;
};

int Com (const void *a, const void *b)
{
        struct person *c = (struct person *) a;
        struct person *d = (struct person *) b;
        return (*c).id - (*d).id;
}

// int Com (const person *a, const person *b)
// {
//         return a->id - b->id;
// }

void starbar(void)//
{
    int count;
    for (count = 1; count <= WIDTH; count++)
    putchar('*');
    putchar('\n');
}
void starbar2(void)//
{
    int count2;
    for (count2 = 1; count2 <= WIDTH2; count2++)
    putchar('-');
    putchar('\n');
}
int open_file(char *fname)
{
    if(fopen(fname,"r") == NULL)
        return -1;
    return 0;
}

int read_int(int *address)
{
    if(scanf("%d",address) == 0)
        return -1;
    return 0;
}
int read_float(float *address)
{
    if(scanf("%f",address) == 0)
        return -1;
    return 0;
}
int read_string(char *address)
{
    if(scanf("%s",address) == 0)
        return -1;
    return 0;
}
void close_file(FILE *fname)
{
    fclose(fname);
}

int binary_search(const int arr[],int l,int r,int x)
{
    if(r>=l)
    {
        int mid = l + (r - l) / 2;
        /* if the element is present at the middle
           itself*/ 
        if(arr[mid] == x)
            return mid;
        /* if element is smaller than mid ,then
           it can only be present in left subarray*/
        if(arr[mid]>x)
            return binary_search(arr, l, mid - 1, x);
        /* else the element can only be present
           in right subarray*/
        return binary_search(arr, mid + 1, r, x);
    }
    /* we reach here when element is not
       present in array*/
    return -1;
}

void print_by_key(struct person employee[SIZE],int key)
{
    //Given structure and id,this function prints the employee record if it exists
    starbar2();
    printf("Name                       SALARY    ID     \n");
    printf("%-10s %-10s %10d %10d\n", employee[key].name, employee[key].last_name, employee[key].salary, employee[key].id);
    starbar2();
}

//遍历法
int search_lastname(struct person employee[SIZE],int n,char surname[])
{
    for (int i = 0; i < n;i++)
    {
        if(strcmp(employee[i].last_name,surname) == 0)
        {
            return i;
        }
    }
    return -1;
}