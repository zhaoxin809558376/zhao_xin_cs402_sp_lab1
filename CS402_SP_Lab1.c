#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include "CS402_SP_Lab1.h"

int main(int argc,char *argv[])
{
    // load input
    if(argc<2){
        printf("pass filename to read from .. \n");
        return 0;
    }
    char *fname = argv[1];

    // define variable
    char name[MAXNAME], last_name[MAXNAME];
    struct person employee[SIZE];
    int option, employee_id, key, salary, confirm;
    int n = 0, array[SIZE];
    
    // read file
    if(open_file(fname) == -1)
    {
        printf("error reading file.\n");
        return -1;
    }
    FILE *fp = fopen(fname, "r");
    while(!feof(fp))
    {
        fscanf(fp, "%d %s %s %d\n", &employee[n].id, &employee[n].name, &employee[n].last_name, &employee[n].salary);
        array[n] = employee[n].id;
        n++;
    }
    fclose(fp);

    //sort
    qsort(employee, 14, sizeof(struct person), Com);

    //menu
    while (1)
    {
        starbar();
        printf("Employee DB Menu:\n");
        starbar();
        printf("(1) Print the Database\n");
        printf("(2) Lookup by ID\n");
        printf("(3) Lookup by Last Name\n");
        printf("(4) Add an Employee\n");
        printf("(5) Quit\n");
        starbar();
        printf("Enter your option: ");
        read_int(&option);
        //scanf("%d", &option);
        if (option == 1)
        {
            starbar2();
            printf("Name                     SALARY     ID     \n");
            starbar2();
            for (int i = 0; i < n; i++)
            {
                printf("%-10s %-10s %10d %10d\n", &employee[i].name, &employee[i].last_name, employee[i].salary, employee[i].id);
            }
            starbar2();
        }
        else if (option == 2)
        {
            printf("Enter a 6 digit employee id: \n");
            read_int(&employee_id);
            //scanf("%d", &employee_id);
            key = binary_search(array, 0, n, employee_id);
            if (key == -1)
            {
                printf("employee with id %d not found. \n", employee_id);
            }
            else
                print_by_key(employee, key);
        }
        else if (option == 3)
        {
            printf("Enter employee's last name(no extra spaces): \n");
            read_string(&last_name);
            //scanf("%s", &last_name);
            key = search_lastname(employee, n, last_name);
            if (key == -1)
            {
                printf("Employee with last name %s not found in DB. \n", last_name);
            }
            else
                print_by_key(employee, key);
        }
        else if (option == 4)
        {
            printf("Enter the first name of the employee:\n");
            read_string(&name);
            //scanf("%s",&name);
            printf("Enter the last name of the employee:\n");
            read_string(&last_name);
            //scanf("%s",&last_name);
            printf("Enter the salary of the employee (between $30,000 and $150,000):\n");
            read_int(&salary);
            //scanf("%d", &salary);
            printf("Do you wanna to add the following employee to the DB?\n");
            printf("Enter 1 for yes,0 for no\n");
            read_int(&confirm);
            //scanf("%d", &confirm);
            if (confirm == 1)
            {
                if ((salary >= 30000) && (salary <= 150000))
                {
                    int cur_id = employee[n - 1].id;
                    int new_id = cur_id + 1;
                    strcpy(employee[n].name, name);
                    strcpy(employee[n].last_name, last_name);
                    employee[n].salary = salary;
                    employee[n].id = new_id;
                    array[n] = employee[n].id;
                    n++;
                }
                else
                {
                    printf("salary invalid.please enter another salary amount. \n");
                }
            }
            else
            {
                printf("dont wanna to add this person into DB.\n");
            }
        }
        else if (option == 5)
        {
            printf("goodbye\n");
            break;
        }
        else
        {
            printf("Hey,%d is not between 1 and 5.Try again...\n", &option);
            continue;
        }
    }
}